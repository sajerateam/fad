
class AuthToken {
  final List<String> scope;
  final String accessToken;
  final String refreshToken;
  final List<String> resources;
  final List<String> authorities;
  final int accessTokenValiditySeconds;
  final int refreshTokenValiditySeconds;

  AuthToken({
    this.scope,
    this.resources,
    this.authorities,
    this.accessToken,
    this.refreshToken,
    this.accessTokenValiditySeconds,
    this.refreshTokenValiditySeconds,
  });

  AuthToken.fromJson(Map<String, dynamic> json)
      : scope = List<String>.from(json['scope']),
        resources = List<String>.from(json['resources']),
        authorities = List<String>.from(json['authorities']),
        accessToken = json['accessToken'],
        refreshToken = json['refreshToken'],
        accessTokenValiditySeconds = json['accessTokenValiditySeconds'],
        refreshTokenValiditySeconds = json['refreshTokenValiditySeconds'];

  Map<String, dynamic> toJson() =>
      {
        'scope': scope,
        'resources': resources,
        'authorities': authorities,
        'accessToken': accessToken,
        'refreshToken': refreshToken,
        'accessTokenValiditySeconds': accessTokenValiditySeconds,
        'refreshTokenValiditySeconds': refreshTokenValiditySeconds,
      };
}
