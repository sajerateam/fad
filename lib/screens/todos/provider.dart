
import 'package:flutter/material.dart';

class TodosModel with ChangeNotifier {
  final List<Task> _tasks = [
    Task(title: 'test', completed: false),
    Task(title: 'test 2', completed: true)
  ];

  get allTasks => _tasks;
  get incompleteTasks => _tasks.where((todo) => !todo.completed);
  get completedTasks => _tasks.where((todo) => todo.completed);

  void addTodo(Task task) {
    _tasks.add(task);
    notifyListeners();
  }

  void toggleTodo(Task task) {
    final taskIndex = _tasks.indexOf(task);
    _tasks[taskIndex].toggleCompleted();
    notifyListeners();
  }

  void deleteTodo(Task task) {
    _tasks.remove(task);
    notifyListeners();
  }
}

class Task {
  String title;
  bool completed;

  Task({ @required this.title, this.completed = false });

  void toggleCompleted() {
    completed = !completed;
  }
}
