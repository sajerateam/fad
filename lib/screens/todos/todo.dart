import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import '../../drawer.dart';
import './provider.dart';


class ScreenTodo extends StatelessWidget {
  static const String routeName = '/todo';

  final task;

  ScreenTodo({ this.task });

  @override
  Widget build(BuildContext context) {
//    final Task task = ModalRoute.of(context).settings.arguments;
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    print(ModalRoute.of(context).settings);
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => TodosModel()),
        ],
        child: ScreenContent(task: task,)
    );
  }
}

class ScreenContent extends StatelessWidget {
  final task;

  ScreenContent({ this.task });

  @override
  Widget build(BuildContext context) {
    print(task);
    return Scaffold(
        drawer: appDrawer,
        appBar: AppBar(
          title: Text('Todos'),
        ),
        body: MyCustomForm(task: task)
    );
  }
}


// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  final task;

  MyCustomForm({ this.task });

  @override
  MyCustomFormState createState() {
    return MyCustomFormState(task: task);
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  final task;

  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  MyCustomFormState({ this.task });

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            initialValue: task.title,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
            onSaved: (String value) {
              task.title = value;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false
                // otherwise.
                if (_formKey.currentState.validate()) {
                  // If the form is valid, display a Snackbar.
                  Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text('Processing Data')));

                  print(json.encode(task));
                }
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}