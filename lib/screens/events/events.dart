//import 'package:flutter/material.dart';
//import 'package:provider/provider.dart';
//
//import '../../drawer.dart';
//import './provider.dart';
//
//class ScreenEvents extends StatelessWidget {
//  static const String routeName = '/events';
//
//  @override
//  Widget build(BuildContext context) {
//    // The Flutter framework has been optimized to make rerunning build methods
//    // fast, so that you can just rebuild anything that needs updating rather
//    // than having to individually change instances of widgets.
//    return MultiProvider(
//        providers: [
//          ChangeNotifierProvider(create: (_) => EventsModel()),
//        ],
//        child: ScreenContent()
//    );
//  }
//}
//
//
//class ScreenContent extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//        drawer: appDrawer,
//        appBar: AppBar(
//          // Here we take the value from the MyHomePage object that was created by
//          // the App.build method, and use it to set our appbar title.
//          title: Text('Events'),
//        ),
//        body: AllEvents()
//    );
//  }
//}
//
//class AllEvents extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      child: Consumer<EventsModel>(
//        builder: (context, list, child) => EventList(
//          list: list.all,
//        ),
//      ),
//    );
//  }
//}
//
//class EventList extends StatelessWidget {
//  final List<Event> list;
//
//  EventList({ @required this.list });
//
//  @override
//  Widget build(BuildContext context) {
//    return RefreshIndicator(
//      onRefresh: () {
//        print('refresh');
//        return Future.delayed(Duration(seconds: 2), () => print('Update list'));
//      },
//      child: ListView.builder(
//        itemCount: list.length,
//        itemBuilder: (context, index) {
//          return EventListItem(event: list[index]);
//        },
//      ),
//    );
//  }
//}
//
//class EventListItem extends StatelessWidget {
//  final Event event;
//
//  EventListItem({ @required this.event });
//
//  @override
//  Widget build(BuildContext context) {
//    return ListTile(
//      title: Text(event.name),
//      onTap: () {
//        print('Click ${event.name}');
//      },
//    );
//  }
//}
