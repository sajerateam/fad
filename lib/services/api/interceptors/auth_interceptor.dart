
// Package
import 'package:dio/dio.dart';

// Services
import '../../storage.dart';

// Constants
import '../../../constants/auth.dart';

class AuthInterceptor extends InterceptorsWrapper {
  Dio previous;
  Dio publicAPI = Dio();

  AuthInterceptor(this.previous);

  // TODO Api request to logout
  logout() async {
    await storage.clearStorage();
    throw DioError(error: 'Token not found. Please do Login.');
  }

  @override
  onRequest(RequestOptions options) async {
    // get [token] from shared or localStorage or Redis Or Sqflite
    String accessToken = await storage.getAccessToken();

    print(accessToken);
//    if (accessToken == null) {
//      await logout();
//    }

    options.headers[AUTH_HEADER] = "$AUTH_BEARER $accessToken";
    return options;
  }

  @override
  onResponse(Response response) async {
    print("AuthInterceptor onResponse <-- END AUTH HTTP");

    return super.onResponse(response);
  }

  @override
  onError(DioError error) async  {
    print("AuthInterceptor onError <-- AUTH Error -->");
    print(error.error);
    print(error.message);

    print(error);

    print('onError ---------------------------------------------------------------------------------------');

    RequestOptions options = error.request;
    // NOTE support request may get 401 (JAVA Spring is fucking genius ...) we must skip restoring for that case
    // TODO !/sign-out|\/oauth\/token/.test(error.config.url)
    if (error.response?.statusCode == 401) {
      // If the token has been updated, repeat directly.
      String accessToken = await storage.getAccessToken();
      String token = "$AUTH_BEARER $accessToken";

      print('onError --------------------------------------------------------------------------------------- $accessToken');

      if (token != options.headers[AUTH_HEADER]) {
        options.headers[AUTH_HEADER] = token;
        return previous.request(options.path, options: options);
      }
      print('lock --------------------------------------------------------------------------------------- 1');

      // Lock to block the incoming request until the token updated
      previous.lock();
//      previous.interceptors.responseLock.lock();
//      previous.interceptors.errorLock.lock();
      print('lock --------------------------------------------------------------------------------------- 2');

      try {
        // GET the [refresh token] from shared or LocalStorage or ....
        String refreshToken = await storage.getRefreshToken();
        print('lock --------------------------------------------------------------------------------------- 3');

        print(refreshToken);
        print("${options.baseUrl}/auth/token/refresh");

        Response responseRefresh = await publicAPI.post(
            "${options.baseUrl}/auth/token/refresh",
            data: '{ "refreshToken": "$refreshToken" }'
        );
        print(responseRefresh);
        print('lock --------------------------------------------------------------------------------------- 4');

        //update token based on the new refresh token
        options.headers[AUTH_HEADER] = "$AUTH_BEARER ${responseRefresh.data['token']}";

        // Save the new token on shared or LocalStorage
        await storage.saveAccessToken(responseRefresh.data[ACCESS_TOKEN]);
        await storage.saveRefreshToken(responseRefresh.data[REFRESH_TOKEN]);
        print('lock --------------------------------------------------------------------------------------- 5');

        previous.unlock();
//        previous.interceptors.responseLock.unlock();
//        previous.interceptors.errorLock.unlock();

        // repeat the request with a new options
        return previous.request(options.path, options: options);

      } catch (e) {
        logout();
      }
    }
    return super.onError(error);
  }
}