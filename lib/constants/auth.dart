
const AUTH_STORE = 'sAuth';
const AUTH_BEARER = 'Bearer';
const AUTH_HEADER = 'Authorization';
const ACCESS_TOKEN = 'accessToken';
const REFRESH_TOKEN = 'refreshToken';