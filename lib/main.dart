
import 'package:flutter/material.dart';

import './screens/todos/todos.dart';
import './screens/todos/todo.dart';
import './screens/home/home.dart';
import './screens/info.dart';
import './screens/signin/signin.dart';
//import './screens/events/events.dart';
import './screens/track/track.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: ScreenHome.routeName,
//      routes: {
//        ScreenHome.routeName: (context) => ScreenHome(),
//        ScreenHome.routeName: (context) => ScreenInfo(),
//        ScreenTodos.routeName: (context) => ScreenTodos(),
//        ScreenTodo.routeName: (context) => ScreenTodo(),
//      }
      // Provide a function to handle named routes. Use this function to
      // identify the named route being pushed, and create the correct
      // screen.
      onGenerateRoute: RouteGenerater.generateRoute,
      navigatorKey: GlobalKey(),
    );
  }
}

class RouteGenerater {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ScreenTrack.routeName:
        final page = ScreenTrack();
        return MaterialPageRoute(builder: (context) => page);

//      case ScreenEvents.routeName:
//        final page = ScreenEvents();
//        return MaterialPageRoute(builder: (context) => page);

      case ScreenSignIn.routeName:
        final page = ScreenSignIn();
        return MaterialPageRoute(builder: (context) => page);

      case ScreenHome.routeName:
        final page = ScreenHome();
        return MaterialPageRoute(builder: (context) => page);

      case ScreenInfo.routeName:
        final page = ScreenInfo();
        return MaterialPageRoute(builder: (context) => page);

      case ScreenTodos.routeName:
        final page = ScreenTodos();
        return MaterialPageRoute(builder: (context) => page);

      case ScreenTodo.routeName:
        print(settings.arguments);
        final page = ScreenTodo(task: settings.arguments);
        return MaterialPageRoute(builder: (context) => page);

      default:
        return MaterialPageRoute(
          builder: (context) => Center(
            child: Text('404'),
          )
        );
    }
  }

}


