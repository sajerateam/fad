
import 'package:flutter/material.dart';

class CounterBloc with ChangeNotifier {
  int _count = 0;
  int get count => _count;

  void set count(int value) {
    _count = value;
    notifyListeners();
  }
}