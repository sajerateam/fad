import './provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './models.dart';
import '../../drawer.dart';

class ScreenSignIn extends StatelessWidget {
  static const String routeName = '/sign-in';

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => AuthProvider()),
        ],
        child: Scaffold(
          drawer: appDrawer,
          appBar: AppBar(
            title: Text('Sign In'),
          ),
          body: LoginForm(),
        )
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _usernameController = TextEditingController(
//      'bbtestb@test.com'
    text: 'bbtestb@test.com'
  );
  final _passwordController = TextEditingController(
      text: '23satan'
  );
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    _onLoginButtonPressed() {
        if (_formKey.currentState.validate()) {
          print('validated');
          var data = SignIn(_usernameController.text, _passwordController.text);
          authProvider.signIn(data);
        }
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
//              initialValue: 'bbtestb@test.com',
              keyboardType: TextInputType.text,
              validator: (text) {
                if (text == null || text.isEmpty) {
                  return 'Username is empty';
                }
                return null;
              },
              decoration: InputDecoration(labelText: 'Username'),
              controller: _usernameController,
            ),
            TextFormField(
//              initialValue: '23satan',
              keyboardType: TextInputType.emailAddress,
              validator: (text) {
                if (text == null || text.isEmpty) {
                  return 'Password is empty';
                }
                return null;
              },
              decoration: InputDecoration(labelText: 'Password'),
              controller: _passwordController,
              obscureText: true,
            ),
            RaisedButton(
              onPressed: _onLoginButtonPressed,
              child: Text('Login'),
            ),
          ],
        ),
      ),
    );
  }
}