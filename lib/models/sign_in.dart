
class SignIn {
  final String username;
  final String password;
  final String client = 'patient_application';

  SignIn(this.username, this.password);

  Map<String, dynamic> toJson() =>
      {
        'client': client,
        'username': username,
        'password': password,
      };
}

class HealthStatus {
  final String status;

  HealthStatus({ this.status });

  HealthStatus.fromJson(Map<String, dynamic> json)
      : status = json['status'];

  Map<String, dynamic> toJson() =>
      {
        'status': status,
      };
}