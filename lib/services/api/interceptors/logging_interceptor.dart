
// Packages
import 'package:dio/dio.dart';

class LoggingInterceptor extends Interceptor{

  @override
  Future onRequest(RequestOptions options) {
    print(
        'LoggingInterceptor onRequest' + '\n' +
        'method' + options.method      + '\n' +
        'path'   + options.path        + '\n' +
        'path'   + options.contentType + '\n'
    );
    return super.onRequest(options);
  }

  @override
  Future onResponse(Response response) {
    print(
        'LoggingInterceptor onResponse'                   + '\n' +
            'statusCode' + response.statusCode.toString() + '\n' +
            'path'       + response.request.method        + '\n' +
            'path'       + response.request.path          + '\n'
            'data'       + response.data.toString()
    );
    print(response.data.toString());
    print("LoggingInterceptor onResponse <-- END HTTP");

    return super.onResponse(response);
  }

  @override
  Future onError(DioError err) {
    print("LoggingInterceptor onError <-- Error -->");
    print(err.error);
    print(err.response);
    print(err.request.toString());
    return super.onError(err);
  }

}