
class User {
  final int id;
  final List roles;
  final String name;
  final bool enabled;
  final String username;
  final List permissions;
  final String clinicRole;
  final Object coverImage;

  User({
    this.id,
    this.roles,
    this.name,
    this.enabled,
    this.username,
    this.clinicRole,
    this.permissions,
    this.coverImage,
  });

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        roles = json['roles'],
        enabled = json['enabled'],
        username = json['username'],
        clinicRole = json['clinicRole'],
        permissions = json['permissions'],
        coverImage = json['coverImage'];

  Map<String, dynamic> toJson() =>
      {
        'id': id,
        'roles': roles,
        'enabled': enabled,
        'username': username,
        'coverImage': coverImage,
        'clinicRole': clinicRole,
        'permissions': permissions,
      };
}
