import 'dart:io';

// Packages
import 'package:dio/dio.dart';

import './interceptors/interceptor.dart';


class _PrivateApiService {
    Dio _dio;

    final _apiPath = 'https://spotter-api.intelliceed.cf';

    _PrivateApiService() {
        BaseOptions options = BaseOptions(
            baseUrl: _apiPath,
            receiveTimeout: 5000,
            connectTimeout: 5000,
            headers: {
                HttpHeaders.contentTypeHeader: 'application/json',
                HttpHeaders.cacheControlHeader: 'no-cache',
            }
        );
        _dio = Dio(options);
        _dio.interceptors.add(AuthInterceptor(_dio));
    }

    get dio => _dio;
}

class _PublicApiService {
    Dio _dio;

    final _apiPath = 'https://spotter-api.intelliceed.cf';

    _PublicApiService() {
        BaseOptions options = BaseOptions(
            baseUrl: _apiPath,
            receiveTimeout: 5000,
            connectTimeout: 5000,
            headers: {
                HttpHeaders.contentTypeHeader: 'application/json',
                HttpHeaders.cacheControlHeader: 'no-cache',
            }
        );
        _dio = Dio(options);
//        _dio.interceptors.add(LoggingInterceptor());
//        _dio.interceptors.add(DataInterceptor());
    }

    get dio => _dio;
}

var publicAPI = _PublicApiService();
var privateAPI = _PrivateApiService();
