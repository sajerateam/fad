
import './models.dart';


class PlanInfo {
  final List<PlanReference> references;
  final String descriptionForPatient;
  final String goal;
  final String name;
  final int id;

  PlanInfo({
    this.descriptionForPatient,
    this.references,
    this.name,
    this.goal,
    this.id,
  });

  PlanInfo.fromJson(Map<String, dynamic> json)
      : descriptionForPatient = json['descriptionForPatient'],
        references = json['references'].cast<PlanReference>(),
        goal = json['goal'],
        name = json['name'],
        id = json['id'];

  Map<String, dynamic> toJson() =>
      {
        'descriptionForPatient': descriptionForPatient,
        'references': references,
        'name': name,
        'goal': goal,
        'id': id,
      };

}

