import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:sensors/sensors.dart';

class TrackProvider with ChangeNotifier {
  Position position;
  AccelerometerEvent accelerometer;
  UserAccelerometerEvent userAccelerometer;
  GyroscopeEvent gyroscope;

  TrackProvider() {
    initialize();
  }

  initialize() async {
    position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.best);

    getPositionStream()
      .listen((Position position) {
        position = position;
        print(json.encode(position));
    });

    accelerometerEvents.listen((AccelerometerEvent event) {
      accelerometer = event;
      print(json.encode(accelerometer));
    });

    userAccelerometerEvents.listen((UserAccelerometerEvent event) {
      userAccelerometer = event;
      print(json.encode(userAccelerometer));
    });

    gyroscopeEvents.listen((GyroscopeEvent event) {
      gyroscope = event;
      print(json.encode(gyroscope));
    });
  }
}
