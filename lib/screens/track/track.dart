
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './provider.dart';
import '../../drawer.dart';

class ScreenTrack extends StatelessWidget {
  static const String routeName = '/track';

  @override
  Widget build(BuildContext context) {
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return ChangeNotifierProvider<TrackProvider>.value(
        value: TrackProvider(),
        child: ScreenContent()
    );
  }
}

class ScreenContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: appDrawer,
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text('track'),
        ),
        body: Text('track')
    );
  }
}

