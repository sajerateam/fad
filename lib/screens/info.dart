import 'package:flutter/material.dart';

import '../drawer.dart';

class ScreenInfo extends StatelessWidget {
  static const String routeName = '/info';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: appDrawer,
      appBar: AppBar(
        title: Text('Info'),
      ),
      body: Center(
        child: Text('Info Screen'),
      ),
    );
  }
}
