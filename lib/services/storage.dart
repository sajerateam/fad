
// Packages
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

// [Local] Constants
import '../constants/auth.dart';
// [Local] Models
import '../models/models.dart';


// Instance of FlutterSecureStorage
final _storage = FlutterSecureStorage();

class _Storage {
  /// Read access_token from local storage
  getAccessToken() async => await _storage.read(key: ACCESS_TOKEN);
  /// Save access_token to local storage
  saveAccessToken(String token) async => await _storage.write(key: ACCESS_TOKEN, value: token);
  /// Read refresh_token from local storage
  getRefreshToken() async => await _storage.read(key: REFRESH_TOKEN);
  /// Save refresh_token to local storage
  saveRefreshToken(String token) async => await _storage.write(key: REFRESH_TOKEN, value: token);
  /// Save access_token and refresh_token to local storage
  saveTokens(AuthToken data) async {
    saveAccessToken(data.accessToken);
    saveRefreshToken(data.refreshToken);
  }
  /// Clear all local storage
  clearStorage() async => await _storage.deleteAll();
}

final storage = _Storage();
