import 'dart:convert';
import 'package:fad/screens/track/track.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:sensors/sensors.dart';
import '../../services/api/api.dart';
import 'package:dio/dio.dart';

class AccessTokenModel {
  String result;
  bool success;
  String userid;

  AccessTokenModel({ this.result, this.success, this.userid, });
}

class AuthProvider with ChangeNotifier {
  bool auth;
  String userid;
  String result;
  AccessTokenModel user;

  signIn(data) async {
    try{
      var response = await publicAPI.dio.get(
        '/mobile/auth',
        queryParameters: { "email": "bbtestb@test.com", "password": "23satan"},
      );
      print('Success');
      const data = AccessTokenModel(response);
      userid = data.userid;
      result = data.result;
      auth = true;
//      Navigator.pushNamed(null, ScreenTrack.routeName);
    } on DioError catch(e){
      auth = false;
      print('Error');
      print(e);
      print(json.encode(e));
    }
    notifyListeners();
  }
}
