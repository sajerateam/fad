import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../drawer.dart';
import './provider.dart';
import './todo.dart';


class ScreenTodos extends StatelessWidget {
  static const String routeName = '/todos';

  @override
  Widget build(BuildContext context) {
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return ChangeNotifierProvider<TodosModel>.value(
        value: TodosModel(),
        child: ScreenContent()
    );
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => TodosModel()),
      ],
      child: ScreenContent()
    );
  }
}

class ScreenContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: appDrawer,
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text('Todos'),
        ),
        body: AllTasksTab()
    );
  }
}

class AllTasksTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<TodosModel>(
        builder: (context, todos, child) => TaskList(
          tasks: todos.allTasks,
        ),
      ),
    );
  }
}

class TaskList extends StatelessWidget {
  final List<Task> tasks;

  TaskList({@required this.tasks});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: getChildrenTasks(),
    );
  }

  List<Widget> getChildrenTasks() {
    return tasks.map((todo) => TaskListItem(task: todo)).toList();
  }
}

class TaskListItem extends StatelessWidget {
  final Task task;

  TaskListItem({@required this.task});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Checkbox(
        value: task.completed,
        onChanged: (bool checked) {
          Provider.of<TodosModel>(context, listen: false).toggleTodo(task);
        },
      ),
      title: InkWell(
        child: Text(task.title),
        onTap: () {
          print('Click ${task.title}');
          Navigator.pushNamed(
            context,
            ScreenTodo.routeName,
            arguments: task,
          );
        },
      ),
      trailing: IconButton(
        icon: Icon(
          Icons.delete,
          color: Colors.red,
        ),
        onPressed: () {
          Provider.of<TodosModel>(context, listen: false).deleteTodo(task);
        },
      ),
    );
  }
}
