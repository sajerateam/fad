// Packages
import 'package:fad/screens/events/events.dart';
import 'package:fad/screens/signin/signin.dart';
import 'package:fad/screens/track/track.dart';
import 'package:flutter/material.dart';


import 'screens/home/home.dart';
import 'screens/todos/todos.dart';
import './screens/info.dart';

final AppDrawer appDrawer = AppDrawer();

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
//                  Container(
//                    decoration: BoxDecoration(
//                      color: Colors.white,
//                      shape: BoxShape.circle,
//                    ),
//                    child: Image.asset(
//                      'assets/images/maintenance.gif',
//                      fit: BoxFit.fitWidth,
//                      width: 50,
//                      height: 50,
//                    )
//                  ),
                  Text('User name')
                ],
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.pushNamed(context, ScreenHome.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text('Info'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.pushNamed(context, ScreenInfo.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text('Todos'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.pushNamed(context, ScreenTodos.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text('SignIn'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.pushNamed(context, ScreenSignIn.routeName);
            },
          ),
//          ListTile(
//            leading: Icon(Icons.info),
//            title: Text('Events'),
//            onTap: () {
//              Navigator.of(context).pop();
//              Navigator.pushNamed(context, ScreenEvents.routeName);
//            },
//          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text('Track'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.pushNamed(context, ScreenTrack.routeName);
            },
          ),
        ],
      ),
    );
  }
}
