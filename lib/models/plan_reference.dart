
class PlanReference {
  final String description;
  final String name;
  final String url;
  final int order;
  final int id;

  PlanReference({
    this.description,
    this.order,
    this.name,
    this.url,
    this.id,
  });

  Map<String, dynamic> toJson() =>
      {
        'description': description,
        'order': order,
        'name': name,
        'url': url,
        'id': id,
      };

  PlanReference.fromJson(Map<String, dynamic> json)
      : description = json['description'],
        order = json['order'],
        name = json['name'],
        url = json['url'],
        id = json['id'];
}